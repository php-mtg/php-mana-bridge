<?php declare(strict_types=1);

/*
 * This file is part of the php-mtg/php-mana-bridge library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

use PhpMtg\Mana\Mana;
use PHPUnit\Framework\TestCase;

/**
 * ManaTest test file.
 * 
 * @author Anastaszor
 * @covers \PhpMtg\Mana\Mana
 *
 * @internal
 *
 * @small
 */
class ManaTest extends TestCase
{
	
	/**
	 * The object to test.
	 * 
	 * @var Mana
	 */
	protected Mana $_object;
	
	/**
	 * {@inheritDoc}
	 * @see \PHPUnit\Framework\TestCase::setUp()
	 */
	protected function setUp() : void {}
	
}
