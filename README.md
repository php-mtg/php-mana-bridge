# php-mtg/php-mana-bridge

A library that brings the mana js library to the php world.

![coverage](https://gitlab.com/php-mtg/php-mana-bridge/badges/master/pipeline.svg?style=flat-square)
![build status](https://gitlab.com/php-mtg/php-mana-bridge/badges/master/coverage.svg?style=flat-square)


## Installation

The installation of this library is made via composer and the autoloading of
all classes of this library is made through their autoloader.

You must add the asset-packagist repository to your `composer.json` :

```
	"repositories" : [{
			"type" : "composer",
			"url" : "https://asset-packagist.org",
			"name" : "asset-packagist"
		}
	]
```

The repository is mandatory as the npm packages do not appear in the generic
packagist repository. Then :

- Download `composer.phar` from [their website](https://getcomposer.org/download/).
- Run the following command to install this library as dependency :
- `php composer.phar php-mtg/php-mana-bridge ^8`


## Basic Usage

```php

use PhpMtg\Mana\Mana;

$symb = Mana::findByCode('w'); // returns a white mana symbol
(string) $symb->getIconHtml(); // <i class="ms ms-w"></i>

```

For the icon to appear on an html page, you have to publish the css and font
files (or uses a cdn like jsdeliver, as specified in the original readme 
of [the mana-font library](https://github.com/andrewgioia/Mana)).

The methods `Mana::getCssFilePath()` and `Mana::getFontFilePaths()`
are made for this task, as they point to the existing files in the mana-font
library, under your vendor directory (as created by composer).


## License

Proprietary
