<?php declare(strict_types=1);

/*
 * This file is part of the php-mtg/php-mana-bridge library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpMtg\Mana;

use PhpExtended\Html\HtmlAbstractNodeInterface;
use PhpExtended\Html\HtmlAttribute;
use PhpExtended\Html\HtmlSingleNode;
use ReflectionException;

/**
 * Mana enum file.
 * 
 * This enum represents a symbol for the Magic: the Gathering game.
 * 
 * @author Anastaszor
 */
enum Mana : string
{
	
	/**
	 * Gets the absolute path on your system for the css file given by the
	 * mana-font library.
	 * 
	 * @param boolean $dev (if true, use the non-minified version)
	 * @return string
	 */
	public static function getCssFilePath(bool $dev = false) : string
	{
		$cssdir = \dirname(__DIR__, 2).\DIRECTORY_SEPARATOR.'npm-asset'.\DIRECTORY_SEPARATOR.'mana-font'.\DIRECTORY_SEPARATOR.'css';
		
		return $cssdir.\DIRECTORY_SEPARATOR.'mana'.((!$dev) ? '.min' : '').'.css';
	}
	
	/**
	 * Gets the absolute paths on your system for the font files given by the
	 * mana-font library.
	 * 
	 * @param string $ext (if found, returns those specific files only)
	 * @return array<integer, string>
	 */
	public static function getFontFilePaths(?string $ext = null) : array
	{
		$fontdir = \dirname(__DIR__, 2).\DIRECTORY_SEPARATOR.'npm-asset'.\DIRECTORY_SEPARATOR.'mana-font'.\DIRECTORY_SEPARATOR.'fonts';
		
		switch($ext)
		{
			case 'eot':
			case 'svg':
			case 'ttf':
			case 'woff':
				return [
					$fontdir.\DIRECTORY_SEPARATOR.'mana.'.$ext,
					$fontdir.\DIRECTORY_SEPARATOR.'mplantin.'.$ext,
				];
			
			case 'woff2':
				return [
					$fontdir.\DIRECTORY_SEPARATOR.'mana.'.$ext,
				];
		}
		
		return [
			$fontdir.\DIRECTORY_SEPARATOR.'mana.eot',
			$fontdir.\DIRECTORY_SEPARATOR.'mana.svg',
			$fontdir.\DIRECTORY_SEPARATOR.'mana.ttf',
			$fontdir.\DIRECTORY_SEPARATOR.'mana.woff',
			$fontdir.\DIRECTORY_SEPARATOR.'mana.woff2',
			$fontdir.\DIRECTORY_SEPARATOR.'mplantin.eot',
			$fontdir.\DIRECTORY_SEPARATOR.'mplantin.svg',
			$fontdir.\DIRECTORY_SEPARATOR.'mplantin.ttf',
			$fontdir.\DIRECTORY_SEPARATOR.'mplantin.woff',
		];
	}
	
	/**
	 * Gets the given keyrune if the set code is found in the library, or the
	 * default one otherwise.
	 * 
	 * @param string $code
	 * @param Mana $default
	 * @return ?Mana
	 * @throws ReflectionException
	 */
	public static function findByCode(string $code, ?Mana $default = null) : ?Mana
	{
		$code = \strtr((string) \mb_strtolower($code), ['_' => '-']);
		
		foreach(static::cases() as $mana)
		{
			if($mana->value === $code)
			{
				return $mana;
			}
		}
		
		return $default;
	}

	/**
	 * Gets the icon html node. This can be easily transformed into an html
	 * string with the __toString() method.
	 * 
	 * @return HtmlAbstractNodeInterface
	 */
	public function getIconHtml() : HtmlAbstractNodeInterface
	{
		return new HtmlSingleNode('i', [new HtmlAttribute('class', 'ms ms-'.$this->value)]);
	}

	case W = 'w';

	case U = 'u';

	case B = 'b';

	case R = 'r';

	case G = 'g';

	case C = 'c';

	case _0 = '0';

	case _1 = '1';

	case _2 = '2';

	case _3 = '3';

	case _4 = '4';

	case _5 = '5';

	case _6 = '6';

	case _7 = '7';

	case _8 = '8';

	case _9 = '9';

	case _10 = '10';

	case _11 = '11';

	case _12 = '12';

	case _13 = '13';

	case _14 = '14';

	case _15 = '15';

	case _16 = '16';

	case _17 = '17';

	case _18 = '18';

	case _19 = '19';

	case _20 = '20';

	case X = 'x';

	case Y = 'y';

	case Z = 'z';

	case P = 'p';

	case S = 's';

	case E = 'e';

	case TAP = 'tap';

	case UNTAP = 'untap';

	case TAP_ALT = 'tap-alt';

	case CHAOS = 'chaos';

	case _1_2 = '1-2'; // 1/2

	case INFINITY = 'infinity';

	case _100 = '100';

	case _1000000 = '1000000';

	case ARTIFACT = 'artifact';

	case CREATURE = 'creature';

	case ENCHANTMENT = 'enchantment';

	case INSTANT = 'instant';

	case LAND = 'land';

	case PLANESWALKER = 'planeswalker';

	case SORCERY = 'sorcery';

	case MULTIPLE = 'multiple';

	case LOYALTY_UP = 'loyalty-up';

	case LOYALTY_DOWN = 'loyalty-down';

	case LOYALTY_ZERO = 'loyalty-zero';

	case LOYALTY_START = 'loyalty-start';

	case FLASHBACK = 'flashback';

	case DFC_NIGHT = 'dfc-night';

	case DFC_DAY = 'dfc-day';

	case DFC_SPARK = 'dfc-spark';

	case DFC_IGNITE = 'dfc-ignite';

	case DFC_MOON = 'dfc-moon';

	case DFC_EMRAKUL = 'dfc-emrakul';

	case DFC_ENCHANTMENT = 'dfc-enchantment';

	case POWER = 'power';

	case TOUGHNESS = 'toughness';

	case ARTIST_BRUSH = 'artist-brush';

	case ARTIST_NIB = 'artist-nib';

	case SAGA = 'saga';

	case ACORN = 'acorn';

	case GUILD_AZORIUS = 'guild-azorius';

	case GUILD_BOROS = 'guild-boros';

	case GUILD_DIMIR = 'guild-dimir';

	case GUILD_GOLGARI = 'guild-golgari';

	case GUILD_GRUUL = 'guild-gruul';

	case GUILD_IZZET = 'guild-izzet';

	case GUILD_ORZHOV = 'guild-orzhov';

	case GUILD_RAKDOS = 'guild-rakdos';

	case GUILD_SELESNYA = 'guild-selesnya';

	case GUILD_SIMIC = 'guild-simic';

	case CLAN_ABZAN = 'clan-abzan';

	case CLAN_JESKAI = 'clan-jeskai';

	case CLAN_MARDU = 'clan-mardu';

	case CLAN_SULTAI = 'clan-sultai';

	case CLAN_TEMUR = 'clan-temur';

	case CLAN_ATARKA = 'clan-atarka';

	case CLAN_DROMOKA = 'clan-dromoka';

	case CLAN_OJUTAI = 'clan-ojutai';

	case CLAN_KOLAGHAN = 'clan-kolaghan';

	case CLAN_SILUMGAR = 'clan-silumgar';

	case POLIS_SETESSA = 'polis-setessa';

	case POLIS_AKROS = 'polis-akros';

	case POLIS_MELETIS = 'polis-meletis';
	
}
